import React from 'react';
import './App.css';
import { BrowserRouter as Router } from 'react-router-dom';
import SideNav from './components/SideNav';
import PrincipalContent from './views/PrincipalContent';

function App() {
	return (
		<Router>
			<SideNav />
			<PrincipalContent />
		</Router>
	);
}

export default App;
