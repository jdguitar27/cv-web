import React from 'react';
import '../App.css';
import Title from '../components/Title';
/**
 * Get all the information from data.json file that contains static data.
 */

function Experience() {
    return (
        <div>
            <Title text="Sofos C.A" />
            <ul className="py-3 list-unstyled">
                <li><h5 className="d-inline-block text-secondary">Desde:</h5> Noviembre de 2018</li>
                <li><h5 className="d-inline-block text-secondary">Hasta:</h5> Actualidad</li>
                <li><h5 className="d-inline-block text-secondary">Cargo:</h5> Analista de Sistemas</li>
                <li><h5 className="d-inline-block text-secondary">Tareas Realizadas</h5></li>
                <li className="py-3 text-justify">
                    Encargado de desarrollar la universidad corporativa de la empresa con Moodle,
                    extender las funcionalidades de Moodle para crear programas de formación acorde
                    a las necesidades de la empresa.<br/><br/>
                    Diseñar la base de datos del proyeto.<br/><br/>
                    Administrar el repositorio de desarrollo del proyecto y crear la metodología a seguir en el repositorio
                    en con respecto a la creación de nuevas funcionalidades y creación de ramas de desarrollo (gitflow).<br/><br/>
                    Administrar los servidores y bases de datos de desarrollo y producción de la plataforma.<br/><br/>
                    Desarrollo de la API para interactuar con Moodle y las nuevas tablas en la base de datos. Desarrollo realizado con PHP.<br/><br/>
                    Creación de las vistas para los módulos adinistrativos de la plataforma con UIkit.<br/><br/>
                    Realización de instalaciones de plataformas Moodle a clientes.
                </li>
            </ul>

            <Title text="Factory Soft Venezuela C.A." />
            <ul className="py-3 list-unstyled">
                <li><h5 className="d-inline-block text-secondary">Desde:</h5> Abril de 2018</li>
                <li><h5 className="d-inline-block text-secondary">Hasta:</h5> Agosto de 2018</li>
                <li><h5 className="d-inline-block text-secondary">Cargo:</h5> Analista de Sistemas</li>
                <li><h5 className="d-inline-block text-secondary">Tareas Realizadas</h5></li>
                <li className="py-3 text-justify">
                    Realización de complementos y extensiones para su software ERP con diferentes funcionalidades,
                    dependiendo de lo requerido, tento front-end como back-end.<br/><br/>
                    Administrar bases de datos y manejar alta cantidad de datos mediante consultas SQL.<br/><br/>
                    Encargado del desarrollo de diversas pantallas para la aplicación móvil de la empresa,
                    y desarrollo de API para conectar la base de datos local con la aplicación, utilizando Kotlin. <br/><br/>
                </li>
            </ul>
        </div>
    );
}

export default Experience;
