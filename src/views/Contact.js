import React from 'react';
import '../App.css';
import Title from '../components/Title';
/**
 * Get all the information from data.json file that contains static data.
 */

function Contact() {
    return (
        <div>
            <Title text="Información de Contacto" />
            <ul className="py-3 list-unstyled">
                <li><h6 className="d-inline-block text-dark">Correo: jose27duran08@gmail.com</h6></li>
                <li><h6 className="d-inline-block text-dark">Teléfono: 0414 585 1512</h6></li>
            </ul>
        </div>
    );
}

export default Contact;
