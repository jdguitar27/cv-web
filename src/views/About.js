import React, { useState } from 'react';
import '../App.css';
import Title from '../components/Title';
import {
    MdFace,
    MdSchool
} from 'react-icons/md';
import {
    FaScroll
} from 'react-icons/fa'

/**
 * Get all the information from data.json file that contains static data.
 */
const data = require('../data.json');

function About() 
{
    const [education] = useState(data.education);
    const [certifications] = useState(data.certifications);

    const printTitle = (institution) => {
        if (institution.title !== undefined) {
            return (
                <React.Fragment>
                    <br />
                    Titulo obtenido: {institution.title}
                </React.Fragment>
            );
        }
    };

    const printInstitution = (institution) => {
        return (
            <li key={institution.id}>
                {institution.description}
                {printTitle(institution)}
            </li>
        );
    };

    const printEducation = () => {
        return education.map((edu, index) => {
            return (
                <React.Fragment key={index}>
                    <h5 className="text-dark">{edu.type}:</h5>
                    <ul>
                        {edu.institutions.map((institution) => printInstitution(institution))}
                    </ul>
                </React.Fragment>
            );
        });
    };

    const printCertifications = () => {
        return certifications.map((certification) => {
            var {title, description, institution, expeditionDate, url} = certification;

            if(institution !== "")
                institution = <React.Fragment> {institution} <br/></React.Fragment>; 
            
            if(description !== "")
                description = <React.Fragment> {description} <br/></React.Fragment>;

            if(expeditionDate !== "")
                expeditionDate = <React.Fragment> Fecha de expedición: {expeditionDate} <br/></React.Fragment>;
            
            if(url !== "")
                url = <a target="_blank" rel="noopener noreferrer" href={url}>Ver certificación...</a>
                
            return(
                <React.Fragment>
                    <h5 className="text-dark">{title}</h5>
                    {institution}
                    {description}
                    {expeditionDate}
                    {url}
                    <br/>
                    <br/>
                </React.Fragment>
            );
        });
    };

    return (
        <div>
            <Title text="Acerca de mí" icon={MdFace} />
            <p className="text-justify py-3">
                Nombres: José Enrique<br/>
                Apellidos: Durán García<br/>
                Nacimiento: San Joaquín - Carabobo<br/>
                Fecha de Nacimiento: 27 de Agosto de 1997<br/>
                <br/>
                Soy un estudiante de computación de la Universidad de Carabobo, me encanta la programación
                y todo el mundo de la tecnología en general. Disfruto mucho programar debido a que se puede
                crear cualquier cosa que quieras con el conocimiento adecuado. <br/><br/>
                Soy un aficionado de los videojuegos y el diseño 3D, especialmente la escultura 3D
                que practico como hobbie usando Blender para crear mis propios personajes.

            </p>

            <Title text="Estudios" icon={MdSchool} />
            <div className="py-3">{printEducation()}</div>

            <Title text="Cursos y certificaciones" icon={FaScroll} />
            <div className="py-3">{printCertifications()}</div>
        </div>
    );
}

export default About;
