import React from 'react';
import '../App.css';
import Banner from '../components/Banner';
import About from './About';
import Skills from './Skills';
import Experience from './Experience';
// import Home from './Home';
import {
  Container
} from 'reactstrap';
import { Switch, Route } from 'react-router-dom';
import PerfectScrollbar from 'react-perfect-scrollbar';
import 'react-perfect-scrollbar/dist/css/styles.css';
import Contact from './Contact';

function PrincipalContent() {
  return (
    <div className="w-100 vh-100">
      <PerfectScrollbar className="m-0 p-0" options={{ swipeEasing: true, wheelSpeed: .3, maxScrollbarLength: 150 }}>
        <Banner />
        <Container className="px-5 py-4">
          <Switch>
            {/* <Route path="/" exact component={Home} /> */}
            <Route path="/" exact component={About} />
            <Route path="/skills" component={Skills} />
            <Route path="/experience" component={Experience} />
            <Route path="/contact" component={Contact} />
          </Switch>
        </Container>
      </PerfectScrollbar>
    </div>
  );
}

export default PrincipalContent;
