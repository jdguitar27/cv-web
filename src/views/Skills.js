import React from 'react';
import '../App.css';
import Title from '../components/Title';
import SkillBar from '../components/SkillBar';
import {
  MdList,
  MdWeb,
  MdDashboard,
  MdPhoneAndroid,
  MdDns,
  MdStorage,
  MdApps,
  MdBugReport
} from 'react-icons/md';
import {
  FaCode,
  FaJsSquare,
  FaHtml5,
  FaCss3,
  FaNodeJs,
  FaReact,
  FaBootstrap,
  FaUikit,
  FaPhp,
  FaJava,
  FaDatabase
} from 'react-icons/fa';

function Skills() {
  return (
    <div>
      <Title text="Habilidades" icon={MdList} />
      <div className="py-3">
        <SkillBar colorIconCls="color-1" text="Desarrollo Web" color="primary" icon={MdWeb} value="95" />
        <SkillBar colorIconCls="color-1" text="Front-end" color="warning" icon={MdDashboard} value="90" />
        <SkillBar colorIconCls="color-1" text="Back-End" color="success" icon={MdDns} value="85" />
        <SkillBar colorIconCls="color-1" text="Desarrollo Móvil" color="color-1" icon={MdPhoneAndroid} value="50" />
        <SkillBar colorIconCls="color-1" text="Bases de Datos" color="color-6" icon={MdStorage} value="70" />
        <SkillBar colorIconCls="color-1" text="POO" color="color-2" icon={MdApps} value="75" />
        <SkillBar colorIconCls="color-1" text="Depuración" color="color-5" icon={MdBugReport} value="80" />
      </div>

      <Title text="Lenguajes y Frameworks" icon={FaCode} />
      <div className="py-3">
        <SkillBar text="HTML" color="color-5" colorIconCls="text-dark" icon={FaHtml5} value="75" />
        <SkillBar text="CSS" color="color-5" colorIconCls="text-dark" icon={FaCss3} value="68" />
        <SkillBar text="Bootstrap" color="color-5" colorIconCls="text-dark" icon={FaBootstrap} value="80" />
        <SkillBar text="UikitCss" color="color-5" colorIconCls="text-dark" icon={FaUikit} value="75" />
        <SkillBar text="Javascript" color="color-5" colorIconCls="text-dark" icon={FaJsSquare} value="90" />
        <SkillBar text="NodeJs" color="color-5" colorIconCls="text-dark" icon={FaNodeJs} value="70" />
        <SkillBar text="ExpressJs" color="color-5" colorIconCls="text-dark" icon={FaNodeJs} value="75" />
        <SkillBar text="ReactJs" color="color-5" colorIconCls="text-dark" icon={FaReact} value="80" />
        <SkillBar text="PHP" color="color-5" colorIconCls="text-dark" icon={FaPhp} value="70" />
        <SkillBar text="Java" color="color-5" colorIconCls="text-dark" icon={FaJava} value="60" />
        <SkillBar text="SQL" color="color-5" colorIconCls="text-dark" icon={FaDatabase} value="70" />
        <SkillBar text="MongoDB" color="color-5" colorIconCls="text-dark" icon={FaDatabase} value="60" />
      </div>

    </div>
  );
}

export default Skills;
