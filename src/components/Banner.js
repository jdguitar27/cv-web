import React from 'react';
import '../App.css';
import {FaAlignLeft} from 'react-icons/fa';

function Banner() {
    return (
        <div className="bg-color-5 banner p-3">
            <h3 className="text-white banner-title d-inline-block w-100">José Durán</h3>
            <button style={{right: '10px'}} type="button" id="sidebarCollapse" class="btn btn-info d-inline-block position-absolute">
                <FaAlignLeft/>
            </button>
            <h4 className="text-white d-none d-sm-inline float-left  ml-3 ml-sm-0">Full Stack Web Developer</h4>
        </div>
    );
}

export default Banner;
