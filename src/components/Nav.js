import React from 'react';
import '../App.css';
import { Link } from 'react-router-dom';
import { Nav, NavItem } from 'reactstrap';
import {
    MdAccountCircle,
    MdPhone,
    MdList
} from "react-icons/md";
import {
    FaYinYang
} from 'react-icons/fa'


function NavBar() {
    return (
        <Nav vertical className="text-left">
            {/* <Link to="/">
                <NavItem className="p-3 sidenav-item">
                    <MdHome size={20} className="d-inline-block mr-2" />
                    <h6 className="d-inline-block align-middle m-0">Inicio</h6>
                </NavItem>
            </Link> */}
            <Link to="/">
                <NavItem className="p-3 sidenav-item">
                    <MdAccountCircle size={20} className="d-inline-block mr-2" />
                    <h6 className="d-inline-block align-middle m-0">Acerca de mí</h6>
                </NavItem>
            </Link>
            <Link to="/skills">
                <NavItem className="p-3 sidenav-item">
                    <FaYinYang size={20} className="d-inline-block mr-2" />
                    <h6 className="d-inline-block align-middle m-0">Habilidades</h6>
                </NavItem>
            </Link>
            <Link to="/experience">
                <NavItem className="p-3 sidenav-item">
                    <MdList size={20} className="d-inline-block mr-2" />
                    <h6 className="d-inline-block align-middle m-0">Experiencia</h6>
                </NavItem>
            </Link>
            <Link to="/contact">
                <NavItem className="p-3 sidenav-item">
                    <MdPhone size={20} className="d-inline-block mr-2" />
                    <h6 className="d-inline-block align-middle m-0">Contacto</h6>
                </NavItem>
            </Link>
        </Nav>
    );
}

export default NavBar;
