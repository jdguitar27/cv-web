import React from 'react';
import '../App.css';

import { Progress } from 'reactstrap';

/**
 * Render a SkillBar with background and an icon.
 */
function SkillBar(props) {
    var icon = "";
    if (props.icon)
        icon = <props.icon size="30" className={props.colorIconCls + " position-absolute"} style={{ right: '-40px', top: '-10px' }} />;
    
    return (
        <React.Fragment>
            <h5 className="text-dark">{props.text}</h5>
            <div className="progress-width mb-3 position-relative">
                <Progress color={props.color} value={props.value} className="bg-dark w-100" />
                {icon}
            </div>
        </React.Fragment>
    );
}

export default SkillBar;
