import React from 'react';
import '../App.css';

class Foto extends React.Component {
    render() {
        return (
            <div className="foto-container p-2 mx-auto my-2">
                <div style={{ borderRadius: '50%' }} className="bg-foto mx-auto"></div>
            </div>
        );
    }
}

export default Foto;