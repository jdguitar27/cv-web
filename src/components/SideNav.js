import React from 'react';
import '../App.css';
import Nav from './Nav';
import Foto from './Foto';
import PerfectScrollbar from 'react-perfect-scrollbar';
import 'react-perfect-scrollbar/dist/css/styles.css';

class SideNav extends React.Component {
    render() {
        return (
            <div className="sidenav">
                <div className=" sidenav bg-dark vh-100 text-center">
                    <PerfectScrollbar options={{swipeEasing: true, wheelSpeed: .1, maxScrollbarLength:200}}>
                        <Foto />
                        <Nav />
                    </PerfectScrollbar>
                </div>
            </div>
        );
    }
}

export default SideNav;