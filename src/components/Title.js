import React from 'react';
import PropTypes from 'prop-types';
import '../App.css';
import {
    Row,
    Col
} from 'reactstrap';

/**
 * Render a title with background and an icon.
 */
function Title(props) 
{
    var icon = "";
    if (props.icon) {
        icon = <Row style={{ width: '50px', height: '50px', top: '-43px', right: '28px' }}
            className="bg-color-4 float-right rounded-circle position-relative d-inline-block text-center align-items-center">
            <Col className="p-0 align-self-center mt-2">
                <props.icon size="35" className="text-white my-auto" />
            </Col>
        </Row>;
    }

    return (
        <div>
            <div className="w-100 py-1 px-2 bg-dark d-inline-block">
                <h4 className="text-white m-0">{props.text}</h4>
            </div>
            {icon}
        </div>
    );
}

Title.propTypes = {
    /**
     * Text for the title.
     * @type String
     */
    text: PropTypes.string,
    icon: PropTypes.func
}

export default Title;
